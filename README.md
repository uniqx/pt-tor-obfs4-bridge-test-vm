Simple test project for running a tor obsf4 bridge in a local VM.

Build VM from scratch (Only tested with vagrant-libvirt on Debian):

    vagrant up --provider libvirt

Generate a bridge line which will be accessible on you local machine (outside the VM):

    vagrant ssh -c 'sudo bash /vagrant/get_bridgeline.sh'


dev notes:
  * handy trick for debugging tls: `openssl s_client -connect 127.0.0.1:9001`
