#! /bin/bash

FP=`awk '{print $2;}' /var/lib/tor/fingerprint`
IP=`cat /home/vagrant/.ipv4 | sed -E 's,^([0-9]+\.[0-9]+\.[0-9]+)\.[0-9]+$,\1.1,'`
BRIDGE_LINE=`cat /var/lib/tor/pt_state/obfs4_bridgeline.txt | tail -1`
BRIDGE_LINE=`echo $BRIDGE_LINE | sed "s/<IP ADDRESS>:<PORT>/$IP:9876/"`
BRIDGE_LINE=`echo $BRIDGE_LINE | sed "s/<FINGERPRINT>/$FP/"`
BRIDGE_LINE=`echo $BRIDGE_LINE | sed "s/^Bridge //"`

echo $BRIDGE_LINE
